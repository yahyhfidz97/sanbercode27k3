



## Final Project

## Kelompok 3

## Anggota Kelompok

- Ahmad Fajar Maulana.
- Patrick Armando.
- Yahya Hafidz.


## Tema Project

- Kelola Penduduk.

## Entity Relationship Diagram

<p align="center"><a href="https://fast-crag-27107.herokuapp.com/" target="_blank"><img src="public/img/erd.png" width="400"></a></p>


## Link Video

[Link video demo program](https://www.youtube.com/watch?v=aTl30hAQS3Y&feature=youtu.be).

[Link Deploy](https://fast-crag-27107.herokuapp.com/).


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
