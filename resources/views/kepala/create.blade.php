@extends('layouts.master')
@section('judul')
     Create Data Kepala
@endsection

@section('content')
    
        <form action="/kepala" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">No. Kepala Keluarga</label>
                <input type="text" class="form-control" name="no_kk" id="no_kk" placeholder="Masukkan No. Kepala Keluarga">
                @error('no_kk')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Nama Kepala Keluarga</label>
                <input type="text" class="form-control" name="kepala" id="kepala" placeholder="Masukkan Nama Kepala Keluarga">
                @error('kepala')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>


@endsection