@extends('layouts.master')

@section('judul')
    List Kepala Keluarga
@endsection

@push('table')
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#kpl").DataTable();
  });
</script>
@endpush

@push('styleTable')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.css">
@endpush

@section('content')

<a href="/kepala/create" class="btn btn-primary my-2">Tambah</a>
        <table id="kpl" class="table table-bordered table-striped table-responsive-sm">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">No Kepala Keluarga</th>
                <th scope="col">Nama Kepala Keluarga</th>
                <th scope="col">Anggota Kelurga</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($kepala as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->no_kk}}</td>
                        <td>{{$value->kepala}}</td>
                        <td><a href="/kepala/{{$value->id}}" class="btn btn-info btn-sm">Show</a></td>
                        <td>
                            
                            <a href="/kepala/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                            <form action="/kepala/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1 btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center"colspan="7">No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection