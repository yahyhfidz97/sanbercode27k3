@extends('layouts.master')



@section('content')


<div class="card-header">
	<h3 class="card-title">
		<i class="fa fa-users"></i> Anggota Keluarga {{$kepala->kepala}}
	</h3>
</div>
<form action="/kepala/{{$kepala->id}}" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}
	<div class="card-body">
		<input type='hidden' class="form-control" id="id_kk" name="id_kk" value="" readonly />

		<div class="form-group row">
			<label class="col-sm-2 col-form-label">No KK | KPl Keluarga</label>
			<div class="col-sm-4">
				<input type="text" class="form-control" id="no_kk" name="no_kk" value="{{$kepala->no_kk}}" readonly />
			</div>
			<div class="col-sm-4">
				<input type="text" class="form-control" id="kepala" name="kepala" value="{{$kepala->kepala}}" readonly />
			</div>
		</div>


		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Anggota</label>
			<div class="col-sm-4">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Tambah Anggota</button>
			</div>
			</div>
			



			<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form action="/kepala/{{$kepala->id}}/createHub" method="POST" enctype="multipart/form-data">
								{{csrf_field()}}
								<div class="form-group">
									<label for="penduduk">Penduduk</label>

									<select class="form-control" id="penduduk" name="penduduk">
										@foreach($penduduk as $pend)
										<option selected="{{$pend->id}}">{{$pend->nik}}</option>
										@endforeach

									</select>
								</div>

								

								<div class="mb-3">
								<label for="exampleInputEmail1" class="form-label">Hubungan</label>
								
                            <input name="hubungan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Hubungan" 
                            value="{{old('hubungan')}}">
                            @if($errors->has('hubungan'))
                            <span class="help-block">{{$errors->first('hubungan')}}</span>
                            @endif
                        	</div>
									


								
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save changes</button>
						</div>
					</div>
				</div>
			</div>

			



		</div>

		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered table-striped">
					<thead class="thead-light">
						<tr>
							<th>NIK</th>
							<th>Nama</th>
							<th>Hub Keluarga</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@foreach($kepala->penduduk as $hub)
						<tr>
							<td>{{$hub->nik}}</td>
							<td>{{$hub->nama}}</td>
							<td>{{$hub->pivot->hubungan}}</td>
							<td>
								<a href="">
									<i class="fa fa-trash"></i>
								</a>
							</td>
							@endforeach

						</tr>


					</tbody>
					</tfoot>
				</table>
			</div>
		</div>

</form>
@endsection