@extends('layouts.master')
@section('judul')
     Edit Data Penduduk No. KK : {{$kepala->no_kk}}
@endsection

@section('content')
        <h2>Edit Data Penduduk {{$kepala->no_kk}}</h2>
        <form action="/kepala/{{$kepala->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">No. KK</label>
                <input type="text" class="form-control" name="no_kk" id="no_kk" value="{{$kepala->no_kk}}" placeholder="Masukkan No. KK">
                @error('no_kk')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Kepala Keluarga</label>
                <input type="text" class="form-control" name="kepala" id="kepala" value="{{$kepala->kepala}}" placeholder="Masukkan Kepala Keluarga">
                @error('kepala')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>


@endsection