@extends('layouts.master')
@section('judul')
    List Penduduk
@endsection

@push('table')

<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#dt").DataTable();
  });
</script>
@endpush

@push('styleTable')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.css">
@endpush

@section('content')

<a href="/penduduk/create" class="btn btn-primary my-2">Tambah</a>
        <table id="dt" class="table table-bordered table-striped table-responsive-sm">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">NIK</th>
                <th scope="col">Tanggal Lahir</th>
                <th scope="col">Umur</th>
                <th scope="col">Alamat</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($penduduk as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->nik}}</td>
                        <td>{{$value->tgl_lahir}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->alamat}}</td>
                        <td>
                            <a href="/penduduk/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/penduduk/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                            <form action="/penduduk/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1 btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center"colspan="7">No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

        
@endsection