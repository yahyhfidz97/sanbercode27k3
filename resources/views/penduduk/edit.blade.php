@extends('layouts.master')
@section('judul')
     Edit Data Penduduk NIK : {{$penduduk->nik}}
@endsection

@section('content')
        <h2>Edit Data Penduduk {{$penduduk->nik}}</h2>
        <form action="/penduduk/{{$penduduk->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">NIK</label>
                <input type="text" class="form-control" name="nik" id="nik" value="{{$penduduk->nik}}" placeholder="Masukkan NIK">
                @error('nik')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" value="{{$penduduk->nama}}" placeholder="Masukkan nama anda">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Tanggal Lahir</label>
                <input type="date" class="form-control" name="tgl_lahir" id="tgl_lahir" value="{{$penduduk->tgl_lahir}}" placeholder="Masukkan umur">
                @error('tgl_lahir')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Umur</label>
                <input type="number" class="form-control" name="umur" id="umur" value="{{$penduduk->umur}}" placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea  name="alamat" class="form-control" id="alamat" cols="30" rows="10">{{$penduduk->alamat}}</textarea>
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>


@endsection