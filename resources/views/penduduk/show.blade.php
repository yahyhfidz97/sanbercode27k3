@extends('layouts.master')



@section('content')
<table class="table table-bordered table-striped table-responsive-sm">
<thead class="thead-light">
    <tr>
	<th>Nomor KK</th>
	<th>Nama Kepala</th>
	<th>Nama Penduduk</th>
	<th>hubungan</th>
	</tr>
</thead>
<tbody> 
@foreach($penduduk->kepala as $hub)
<tr>
    <td>{{$hub->no_kk}}</td>
	<td>{{$hub->kepala}}</td>
	<td>{{$penduduk->nama}}</td>
	<td>{{$hub->pivot->hubungan}}</td>
</tr>
@endforeach
</tbody>
</table>
@endsection