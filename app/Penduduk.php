<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penduduk extends Model
{
    protected $table = "tb_pdd";
    public $timestamps = false;
    protected $fillable = ['nik','nama','tgl_lahir','umur','alamat'];

    public function kepala(){
        return $this->belongsToMany(Kepala::class)->withPivot(['hubungan']);
    }
}
