<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendpala extends Model
{
    public $table = "kepala_penduduk";
    protected $guarded = [];
    public $timestamps = false;
}
