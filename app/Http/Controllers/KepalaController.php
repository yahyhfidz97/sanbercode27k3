<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kepala;
use App\Penduduk;
use App\Pendpala;

class KepalaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     //untuk auth tabel kk
    public function __construct(){
        $this->middleware('auth')->except(['create','store','show','edit','update','destroy']);
    }

    public function index()
    {
        $kepala = Kepala::all();
        return view('kepala.index', compact('kepala'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('kepala.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'no_kk' => 'required',
            'kepala' => 'required',
        ]);

        //dengan ORM
        $kepala = Kepala::create ([
            'no_kk' => $request->no_kk,
            'kepala' => $request->kepala,
        ]);

        return redirect('/kepala');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kepala = Kepala::find($id);
        $penduduk = Penduduk::all();
        $hub = Pendpala::all();
        
        
        return view('kepala.show', ['penduduk' => $penduduk, 'kepala' => $kepala, 'hub' => $hub]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kepala = Kepala::find($id);
        return view('kepala.edit', compact('kepala'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'no_kk' => 'required',
            'kepala' => 'required',
        ]);

        $kepala = Kepala::find($id);
        $kepala->no_kk      = $request->no_kk;
        $kepala->kepala     = $request->kepala;
        $kepala->update();
        
        return redirect('/kepala');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kepala = Kepala::destroy($id);
        return redirect('/kepala');
    }


    public function createHub(Request $request,$idkepala){
        $kepala = \App\Kepala::find($idkepala);
        $kepala->penduduk()->attach($request->penduduk,['hubungan' => $request->hubungan]);

    return redirect('kepala/' .$idkepala);
}

}