<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Penduduk;
use App\Kepala;

class PendudukController extends Controller
{
    //untuk auth tabel pdd
    public function __construct(){
        $this->middleware('auth')->except(['create','store','show','edit','update','destroy']);
    }

    public function index()
    {
        $penduduk = Penduduk::all();
        return view('penduduk.index', compact('penduduk'));
    }

    public function create()
    {
        return view ('penduduk.create');
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'nik' => 'required',
            'nama' => 'required',
            'tgl_lahir' => 'required',
            'umur' => 'required',
            'alamat' => 'required'
        ]);

        //dengan ORM
        $penduduk = Penduduk::create ([
            'nik' => $request->nik,
            'nama' => $request->nama,
            'tgl_lahir' => $request->tgl_lahir,
            'umur' => $request->umur,
            'alamat' => $request->alamat
        ]);

        return redirect('/penduduk');
    }


    public function show($id)
    {
        $penduduk = Penduduk::find($id);
        $kepala = Kepala::all();
        
        
        return view('penduduk.show', ['penduduk' => $penduduk, 'kepala' => $kepala]);
    }


    public function edit($id)
    {
        $penduduk = Penduduk::find($id);
        return view('penduduk.edit', compact('penduduk'));
    }

 
    public function update(Request $request, $id)
    {
        $request->validate([
            'nik' => 'required',
            'nama' => 'required',
            'tgl_lahir' => 'required',
            'umur' => 'required',
            'alamat' => 'required'
        ]);

        $penduduk = Penduduk::find($id);
        $penduduk->nik      = $request->nik;
        $penduduk->nama     = $request->nama;
        $penduduk->tgl_lahir= $request->tgl_lahir;
        $penduduk->umur     = $request->umur;
        $penduduk->alamat   = $request->alamat;
        $penduduk->update();
        
        return redirect('/penduduk');

    }

    public function destroy($id)
    {
        $penduduk = Penduduk::destroy($id);
        return redirect('/penduduk');
    }
}
