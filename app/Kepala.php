<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kepala extends Model
{
    protected $table = "tb_kk";
    
    public $timestamps = false;
    protected $fillable = ['no_kk','kepala'];

    public function penduduk(){
        return $this->belongsToMany(Penduduk::class)->withPivot(['hubungan']);
    }

   
}
