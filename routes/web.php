<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::group(['middleware' => ['auth']], function () {
Route::resource('penduduk', 'PendudukController');

Route::resource('kepala', 'KepalaController');

//insert & delete hubungan
Route::post('kepala/{kepala}', 'KepalaController@createHub');
//Route::get('kepala/{kepala}', 'KepalaController@deleteHub');



// Route::get('/penduduk', 'PendudukController@index');
// Route::get('/penduduk/create', 'PendudukController@create');
// Route::post('/penduduk', 'PendudukController@store');
// Route::get('/penduduk/{penduduk_id}', 'PendudukController@show');
// Route::get('/penduduk/{penduduk_id}/edit', 'PendudukController@edit');
// Route::put('/penduduk/{penduduk_id}', 'PendudukController@update');
// Route::delete('/penduduk/{penduduk_id}', 'PendudukController@destroy');
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
